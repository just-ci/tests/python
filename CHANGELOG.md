## [1.4.3](https://gitlab.com/just-ci/tests/python/compare/v1.4.2...v1.4.3) (2024-03-11)


### Bug Fixes

* run it ([28260e5](https://gitlab.com/just-ci/tests/python/commit/28260e5218b5e8bb67d8463268abb80ab1384c4b))

## [1.4.2](https://gitlab.com/just-ci/tests/python/compare/v1.4.1...v1.4.2) (2024-1-12)


### Bug Fixes

* with fix now ([86054d7](https://gitlab.com/just-ci/tests/python/commit/86054d7421b1a10af7891922a0ecca6a9d522a2f))

## [1.4.1](https://gitlab.com/just-ci/tests/python/compare/v1.4.0...v1.4.1) (2024-1-12)


### Bug Fixes

* hi ([cd20832](https://gitlab.com/just-ci/tests/python/commit/cd208329f42a654817afd5ca0ccdba76c9012742))
* try update ([e6ec535](https://gitlab.com/just-ci/tests/python/commit/e6ec535d3be026b4bac690daeeab13788f4d37ab))

## [1.3.2](https://gitlab.com/just-ci/tests/python/compare/v1.3.1...v1.3.2) (2022-08-03)


### Bug Fixes

* try this ([761660e](https://gitlab.com/just-ci/tests/python/commit/761660e686224c9915d63edd0da3859bf6db7248))
